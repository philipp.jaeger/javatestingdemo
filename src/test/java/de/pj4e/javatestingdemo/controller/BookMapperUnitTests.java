package de.pj4e.javatestingdemo.controller;

import de.pj4e.javatestingdemo.model.BookEntity;
import de.pj4e.javatestingdemo.view.BookDto;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BookMapperUnitTests {

    @Test
    void entityToDto() {
        final BookEntity bookEntity = BookEntity.builder().id(1).author("Test").year(2022).title("Hello World").build();
        final BookDto bookDto = BookMapper.toDto(bookEntity);
        assertEquals("Test", bookDto.author());
        assertEquals("Hello World", bookDto.title());
        assertEquals(2022, bookDto.year());
    }

    @Test
    void dtoToEntity() {
        final BookDto bookDto = new BookDto("Hello World", "Test", 2022);
        final BookEntity bookEntity = BookMapper.toEntity(bookDto);
        assertEquals(0, bookEntity.getId());
        assertEquals("Test", bookEntity.getAuthor());
        assertEquals("Hello World", bookEntity.getTitle());
        assertEquals(2022, bookEntity.getYear());
    }
}
