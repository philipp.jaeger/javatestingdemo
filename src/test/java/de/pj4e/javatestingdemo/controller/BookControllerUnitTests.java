package de.pj4e.javatestingdemo.controller;

import de.pj4e.javatestingdemo.view.BookDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BookControllerUnitTests {

    @Mock
    private BookService bookService;

    @InjectMocks
    private BookController bookController;

    final BookDto book = new BookDto("Test Title", "Santa Claus", 1234);

    // Run this test multiple times with a parameter
    @ParameterizedTest
    // Define the parameter values this test should be run for
    @ValueSource(longs = {1L, 765L, 999999999})
    void whenBookAdded_thenReturnCreated(long id) {
        when(bookService.addBook(book)).thenReturn(Optional.of(id));
        var response = bookController.addBook(book);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("http://localhost/api/v1/book/" + id, Objects.requireNonNull(response.getHeaders().get("Location")).get(0));
    }

    @Test
    void whenBookAddedButExists_thenReturnBadRequest() {
        when(bookService.addBook(book)).thenReturn(Optional.empty());
        var response = bookController.addBook(book);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void whenGetBookById_thenReturnBook() {
        when(bookService.getBookById(1)).thenReturn(Optional.of(book));
        var response = bookController.getBookById(1);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(book.title(), Objects.requireNonNull(response.getBody()).title());
        assertEquals(book.author(), Objects.requireNonNull(response.getBody()).author());
        assertEquals(book.year(), Objects.requireNonNull(response.getBody()).year());
    }

    @Test
    void whenGetUnknownBookById_thenReturnNotFound() {
        when(bookService.getBookById(1)).thenReturn(Optional.empty());
        var response = bookController.getBookById(1);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}
