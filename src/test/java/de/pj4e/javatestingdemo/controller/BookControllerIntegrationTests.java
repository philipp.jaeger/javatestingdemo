package de.pj4e.javatestingdemo.controller;

import de.pj4e.javatestingdemo.model.BookEntity;
import de.pj4e.javatestingdemo.view.BookDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.Objects;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BookControllerIntegrationTests {

    final BookDto book = new BookDto("Test Title", "Santa Claus", 1234);

    @Autowired
    private BookController bookController;

    @Autowired
    private BookService bookService;

    @Autowired
    private BookRepository bookRepository;

    @Test
    void postRequest_thenBookCreated() {
        final var count = bookRepository.count();
        final var response = bookController.addBook(book);
        assertTrue(bookRepository.count() > count);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    void postRequestTwice_thenBadRequest() {
        final var book = new BookDto("Test Title 3", "Santa Claus", 1234);
        bookController.addBook(book);
        final var response = bookController.addBook(book);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void getAllBooks_thenOk() {
        final var response = bookController.getAllBooks();
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void deleteBook_thenAccepted() {
        final var book = new BookDto("Test Title 2", "Santa Claus", 1234);
        final var id = bookService.addBook(book).orElseThrow();
        final var response = bookController.deleteBook(id);
        assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
    }

    @Test
    void deleteUnknownBook_thenBadRequest() {
        final var maxId = StreamSupport.stream(bookRepository.findAll().spliterator(), false).takeWhile(Objects::nonNull).mapToLong(BookEntity::getId).max();
        final var response = bookController.deleteBook(maxId.orElse(1) + 1);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }
}
