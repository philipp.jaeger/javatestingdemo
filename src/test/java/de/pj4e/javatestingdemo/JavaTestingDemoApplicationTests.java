package de.pj4e.javatestingdemo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class JavaTestingDemoApplicationTests {

    @Test
    void contextLoads() {
        // this is fine
    }
}
