package de.pj4e.javatestingdemo.view;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public record BookDto(
        @NotEmpty String title,
        @NotEmpty String author,
        @Min(1) int year
) {
}
