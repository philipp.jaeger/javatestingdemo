package de.pj4e.javatestingdemo.validation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

// Only load this component in deployment
@Profile("deployment")
// This bean should be attached to all controllers
@ControllerAdvice
// Add logger
@Slf4j
public class ValidationExceptionHandler {

    @ExceptionHandler(value = { ConstraintViolationException.class })
    protected ResponseEntity<Object> handleConstraintViolation(RuntimeException ex) {
        log.info("Caught constraint violation");
        return ResponseEntity.unprocessableEntity().body("Request violates constraints: " + ex.getMessage());
    }
}
