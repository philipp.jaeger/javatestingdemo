package de.pj4e.javatestingdemo.controller;

import de.pj4e.javatestingdemo.exceptions.OutOfCookiesException;
import de.pj4e.javatestingdemo.view.BookDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class BookController {

    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/book")
    public ResponseEntity<List<BookDto>> getAllBooks() {
        return ResponseEntity.ok(bookService.getAllBooks());
    }

    @GetMapping("/book/{id}")
    public ResponseEntity<BookDto> getBookById(@PathVariable long id) {
        return ResponseEntity.of(bookService.getBookById(id));
    }

    @GetMapping("/book/year/{year}")
    public ResponseEntity<List<BookDto>> getBooksByYear(@PathVariable int year) {
        return ResponseEntity.ok(bookService.getBooksByYear(year));
    }

    @PostMapping("/book")
    @Operation(summary = "Add a new book")
    @ApiResponse(
            responseCode = "201",
            description = "Book created successfully",
            content = @Content(mediaType = MediaType.ALL_VALUE),
            headers = {
                    @Header(name = "LOCATION", description = "The URI of the newly created book", schema = @Schema(type = MediaType.TEXT_PLAIN_VALUE)),
                    @Header(name = "BOOK_ID", description = "The Id of the newly created book", schema = @Schema(type = MediaType.TEXT_PLAIN_VALUE))
            })
    @ApiResponse(
            responseCode = "400",
            description = "A book with this metadata already exists")
    public ResponseEntity<Object> addBook(@Parameter @RequestBody BookDto book) {
        return bookService.addBook(book)
                .map(id -> {
                    try {
                        return ResponseEntity.created(new URI("http://localhost/api/v1/book/" + id))
                                .header("BOOK_ID", String.valueOf(id))
                                .build();
                    } catch (URISyntaxException e) {
                        throw new OutOfCookiesException(e);
                    }
                })
                .orElse(ResponseEntity.badRequest().build());
    }

    @DeleteMapping("/book/{id}")
    public ResponseEntity<Object> deleteBook(@PathVariable long id) {
        if (bookService.deleteBook(id)) {
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
}
