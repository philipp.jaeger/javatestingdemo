package de.pj4e.javatestingdemo.controller;

import de.pj4e.javatestingdemo.model.BookEntity;
import de.pj4e.javatestingdemo.view.BookDto;

public class BookMapper {

    private BookMapper() {}

    public static BookDto toDto(BookEntity bookEntity) {
        return new BookDto(bookEntity.getTitle(), bookEntity.getAuthor(), bookEntity.getYear());
    }

    public static BookEntity toEntity(BookDto bookDto) {
        return new BookEntity().setYear(bookDto.year()).setTitle(bookDto.title()).setAuthor(bookDto.author());
    }
}
