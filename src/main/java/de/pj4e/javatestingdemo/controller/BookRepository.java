package de.pj4e.javatestingdemo.controller;

import de.pj4e.javatestingdemo.model.BookEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookRepository extends PagingAndSortingRepository<BookEntity, Long> {

    List<BookEntity> findAllByOrderByAuthor();

    List<BookEntity> findByYearOrderByAuthor(int year);

    Optional<BookEntity> findByAuthorAndTitleAndYear(String author, String title, int year);
}
