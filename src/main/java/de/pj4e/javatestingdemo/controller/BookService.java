package de.pj4e.javatestingdemo.controller;

import de.pj4e.javatestingdemo.model.BookEntity;
import de.pj4e.javatestingdemo.view.BookDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<BookDto> getAllBooks() {
        return bookRepository.findAllByOrderByAuthor().stream()
                .map(BookMapper::toDto)
                .toList();
    }

    public Optional<BookDto> getBookById(long id) {
        return bookRepository.findById(id).map(BookMapper::toDto);
    }

    public List<BookDto> getBooksByYear(int year) {
        return bookRepository.findByYearOrderByAuthor(year).stream()
                .map(BookMapper::toDto)
                .toList();
    }

    public Optional<Long> addBook(BookDto book) {
        if (bookRepository.findByAuthorAndTitleAndYear(book.author(), book.title(), book.year()).isPresent()) {
            // book already exists
            return Optional.empty();
        } else {
            BookEntity bookEntity = bookRepository.save(BookMapper.toEntity(book));
            return Optional.of(bookEntity.getId());
        }
    }

    public boolean deleteBook(long id) {
        return bookRepository.findById(id)
                .map(book -> {
                    bookRepository.delete(book);
                    return true;
                }).orElse(false);
    }
}
