package de.pj4e.javatestingdemo.exceptions;

public class OutOfCookiesException extends RuntimeException{

    public OutOfCookiesException(Exception e) {
        super(e);
    }
}
