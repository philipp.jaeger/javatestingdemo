package de.pj4e.javatestingdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaTestingDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaTestingDemoApplication.class, args);
    }

}
