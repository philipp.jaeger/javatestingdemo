package de.pj4e.javatestingdemo.model;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

// Tell JPA to handle this class as a database entity
@Entity
// Tell JPA which table to use
@Table(name = "book")
// Use Lombok to generate getters and setters
@Getter
@Setter
// Make setters return the updated object
@Accessors(chain = true)
// Implements the Builder pattern
@Builder
// Add a constructor that takes no arguments
@NoArgsConstructor
// Add a constructor that requires all fields as arguments
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class BookEntity {

    // Mark this field as the primary key colums of the corresponding db table
    @Id
    // Tell JPA to generate this value automatically
    @GeneratedValue
    // Disable setter for this field
    @Setter(AccessLevel.NONE)
    private long id;

    // Setting max size since JPA uses VARCHAR(255) by default
    @Size(min = 3, max = 255)
    // May not consist of whitespace only
    @NotBlank
    private String title;

    @NotEmpty
    private String author;

    // Specify minimal allowed value
    @Min(1)
    // Custom name since year is a reserved word
    @Column(name = "byear")
    private int year;
}