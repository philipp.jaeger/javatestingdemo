FROM openjdk:17-bullseye

COPY . /app

WORKDIR /app
RUN apt-get update && apt-get install -y maven openjdk-17-jdk
RUN ls
RUN mvn clean package

FROM openjdk:17-bullseye

ENV ARTIFACT=JavaTestingDemo
ENV VERSION=0.0.1-SNAPSHOT
ENV SPRING_PROFILES_ACTIVE=deployment

COPY --from=0 /app/target/$ARTIFACT-$VERSION.jar .

CMD java -jar $ARTIFACT-$VERSION.jar
